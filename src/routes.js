import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Main from './pages/Main';
import User from './pages/User';

const AppNavigator = createStackNavigator(
  {
    Home: Main,
    Usuarios: User,
    initialRouteName: 'Home',
  },
  {
    defaultNavigationOptions: {
      headerTitleAlign: 'center',
      headerBackTitleVisible: false, // IOS
      headerStyle: {
        backgroundColor: '#7159c1',
      },
      headerTintColor: '#FFF',
    },
  }
);

const Routes = createAppContainer(AppNavigator);

export default Routes;

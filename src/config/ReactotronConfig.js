import Reactotron from 'reactotron-react-native';

if (__DEV__) {
  const tron = Reactotron.configure({
    enable: true,
    host: '10.0.0.107',
    // port: 8081,
  })
    .useReactNative()
    .connect();

  console.tron = tron;
  tron.clear();
}
